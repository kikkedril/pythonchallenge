import os
from openpyxl import Workbook

# maak een Workbook object
wb = Workbook()
sheet = wb.active
sheet['A1'] = 1
sheet.cell(row=2, column=2).value = 2

data = [('Id', 'Name', 'Marks'),
        (1, "ABC", 50),
        (2, "CDE", 100)]
for row in data:
    sheet.append(row)


# itereer door de cellen
max_row = sheet.max_row
max_column = sheet.max_column
for i in range(1, max_row + 1):
    for j in range(1, max_column + 1):
        cell_obj = sheet.cell(row=i, column=j)
        print(cell_obj.value, end=' | ')
    print('\n')

# hier opslaan
filepath = "/demo.xlsx"

# workbook opslaan
wb.save(filepath)

# laat het resultaat zien
os.system(filepath)

from openpyxl import load_workbook

load_workbook(filepath)
# de rest is hetzelfde
