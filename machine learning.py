import sklearn
from sklearn.linear_model import LinearRegression

y = [[1], [2], [3], [4], [5]]
x = [100, 200, 300, 400]

reg = LinearRegression().fit(x, y)
reg.predict([7], [2])
