# scikit-learn.org
# https://scikit-learn.org/stable/tutorial/machine_learning_map/index.html

# linear regression
from sklearn.linear_model import LinearRegression

x = [[1], [2], [3], [4], [5]]
y = [100, 200, 300, 400, 500]

reg = LinearRegression().fit(x, y)
print(reg.predict([[7, 20], [2, 50]])) # outputs prices